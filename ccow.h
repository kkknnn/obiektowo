#ifndef CCOW_H
#define CCOW_H
#include "canimal.h"

class CCow:public CAnimal
{
public:
    CCow();
    void giveVoice()const override;

};

#endif // CCOW_H
