#ifndef CANIMAL_H
#define CANIMAL_H
#include <iostream>

class CAnimal
{
public:
    virtual ~CAnimal() = default;
    virtual void giveVoice() const = 0;
    virtual void roaaar() const
    {
        std::cout << "roooaaar" << '\n';
    }




};

#endif // CANIMAL_H
