#ifndef CSHEEP_H
#define CSHEEP_H
#include "canimal.h"

class CSheep: public CAnimal
{
public:
    CSheep();
    void giveVoice()const override;

};

#endif // CSHEEP_H
