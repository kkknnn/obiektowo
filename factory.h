#ifndef FACTORY_H
#define FACTORY_H
#include <vector>
#include "canimal.h"
#include "ccat.h"
#include "cdog.h"
#include "csheep.h"
#include "ccow.h"
#include <random>


class factory
{
public:
    factory()=default;
    ~factory();
    CAnimal * generateAnimals();
};

#endif // FACTORY_H
