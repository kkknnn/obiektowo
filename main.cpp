#include <iostream>
#include "ccat.h"
#include "cdog.h"
#include "csheep.h"
#include "ccow.h"
#include "canimal.h"
#include "factory.h"



enum Species
{
    Cat = 0,
    Cow = 1,
    Sheep = 2,
    Dog = 3
};

namespace FarmManagement
{
void TouchAnimal(const CAnimal * arg)
{

    if ( arg != nullptr)
    {
    arg->giveVoice();
    }
}

CAnimal * getAnimal(Species type)
{
    switch(type)
    {
    case(Cat): return new CCat();
    case(Cow): return new CCow();
    case(Dog): return new CDog();
    case(Sheep): return new CSheep();
    default: return nullptr;
    }
}


}




using namespace std;

int main()
{

    CCow andrzej;
    CDog jurek;
    CSheep wiesiek;
    CCat natka;
    CAnimal * someanimal = FarmManagement::getAnimal(Species::Cat);
    factory farm;
    farm.generateAnimals();
    someanimal->giveVoice();
    std::vector<CAnimal*> Shelter;

    for (int i = 0; i <100 ;i++)
    {
        Shelter.push_back(farm.generateAnimals());

    }

    for (uint i = 0; i < Shelter.size();i++)
    {

        auto tempnum = dynamic_cast<CSheep*>(Shelter[i]);
        if (tempnum!=nullptr)
        {

            std::cout<< "Animal nr " << i << '\n';
            Shelter[i]->giveVoice();

        }

    }

    delete someanimal;
    return 0;
}
