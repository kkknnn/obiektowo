#ifndef CCAT_H
#define CCAT_H
#include <iostream>
#include "canimal.h"

class CCat: public CAnimal
{
public:
    CCat();
    void giveVoice()const override;

};

#endif // CCAT_H
