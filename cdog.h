#ifndef CDOG_H
#define CDOG_H
#include "canimal.h"

class CDog: public CAnimal
{
public:
    CDog();
    void giveVoice()const override;
};

#endif // CDOG_H
